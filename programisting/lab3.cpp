#include "lab3.h"
#include <iostream>
#include <vector>

lab3::cLab3::cLab3() : cLab()
{
	exercises.insert({ 1, &ex1 });
}

void lab3::cEx1::run()
{
	unsigned int n = 0, m = 0;
	std::cout << "enter n:";
	std::cin >> n;
	std::cout << "enter m:";
	std::cin >> m;


	std::vector<std::vector<int>> matrix(n, std::vector<int>(m));
	std::vector<std::vector<int>> tmatrix(m, std::vector<int>(n));

	for (size_t i = 0; i < n; i++)
	{
		for (size_t j = 0; j < m; j++)
		{
			std::cout << "enter " << i+1 << j+1 << " element:";
			std::cin >> matrix[i][j];
		}
	}

	for (size_t i = 0; i < m; i++)
	{
		for (size_t j = 0; j < n; j++)
		{
			tmatrix[i][j] = matrix[j][i];
		}
	}

	for (size_t i = 0; i < m; i++)
	{
		for (size_t j = 0; j < n; j++)
		{
			std::cout << tmatrix[i][j] << " ";
		}
		std::cout << std::endl;
	}
}