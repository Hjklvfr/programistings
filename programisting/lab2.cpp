#include "lab2.h"
#include <iostream>
#include <map>
#include <list>

#define GIVE_ME_FUCKING_NUMBER std::cout << "enter a number: ";int n = 0;std::cin >> n;

lab2::cLab2::cLab2(): cLab()
{
	exercises.insert({ 1, &ex1 });
	exercises.insert({ 4, &ex4 });
	exercises.insert({ 8, &ex8 });
	exercises.insert({ 10, &ex10 });
	exercises.insert({ 11, &ex11 });
}

void lab2::cEx1::run()
{
	GIVE_ME_FUCKING_NUMBER;
	while (n > 0)
	{
		int digit = n % 10;
		n /= 10;

		std::cout << digit;
	}
}

void lab2::cEx4::run()
{
	int suma = 0;
	int proizved = 1;

	GIVE_ME_FUCKING_NUMBER;

	while (n > 0)
	{
		int digit = n % 10;
		n /= 10;

		suma += digit;
		proizved *= digit;
	}

	std::cout << suma << std::endl;
	std::cout << proizved;
}

void lab2::cEx8::run()
{
	GIVE_ME_FUCKING_NUMBER;

	if (n < 1)
	{
		std::cout << "Error";
	}

	long long first = 0;
	long long second = 1;

	std::cout << "1 ";
	for (long long i = 1; i < n; i++)
	{
		long long value = first + second;

		first = second;
		second = value;

		std::cout << value << " ";
	}
}

void lab2::cEx10::run()
{
	std::cout << "enter m: "; int m = 0; std::cin >> m;
	std::cout << "enter n: "; int n = 0; std::cin >> n;
	
	if (n < m)
	{
		return;
	}

	for (int i = m; i <= n; i++)
	{
		std::cout << "divisors of " << i << ": ";
		printDivs(i);
		std::cout << std::endl;
	}
}

void lab2::cEx10::printDivs(int number)
{
	for (int i = 2; i <= number / 2; i++)
		if (number % i == 0) 
			std::cout << i << " ";
}

void lab2::cEx11::run()
{
	GIVE_ME_FUCKING_NUMBER;

	if (n < 3 || n > 27)
	{
		std::cout << "NO";
		return;
	}

	for (size_t i = 1; i < 9; i++)
	{
		for (size_t j = 1; j < 9; j++)
		{
			for (size_t k = 1; k < 9; k++)
			{
				if (i + j + k == n)
				{
					std::cout << i << j << k << " ";
				}
			}
		}
	}
	return;
}
