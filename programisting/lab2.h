#pragma once
#include "exercise.h"
#include "lab.h"

namespace lab2 {
	class cEx1 : public cExercise {
	public:
		virtual void run() override;
	};

	class cEx4 : public cExercise {
	public:
		virtual void run() override;
	};

	class cEx8 : public cExercise {
	public:
		virtual void run() override;
	};

	class cEx10 : public cExercise {
		void printDivs(int number);
	public:
		virtual void run() override;
	};

	class cEx11 : public cExercise {
	public:
		virtual void run() override;
	};

	class cLab2 : public cLab
	{
	private:
		cEx1 ex1;
		cEx4 ex4;
		cEx8 ex8;
		cEx10 ex10;
		cEx11 ex11;
	public:
		cLab2();
	};
}
