#include "lab4.h"
#include "mt.cpp"

lab4::cLab4::cLab4() : cLab()
{
	exercises.insert({ 1, &ex1 });
	exercises.insert({ 2, &ex2 });
	exercises.insert({ 3, &ex3 });
	exercises.insert({ 4, &ex4 });
	exercises.insert({ 5, &ex5 });
	exercises.insert({ 6, &ex6 });
}

void lab4::cEx1::run()
{
	utm mm;
	mm.start("ex1");
}

void lab4::cEx2::run()
{
	utm mm;
	mm.start("ex2");
}

void lab4::cEx3::run()
{
	utm mm;
	mm.start("ex3");
}

void lab4::cEx4::run()
{
	utm mm;
	mm.start("ex4");
}

void lab4::cEx5::run()
{
	utm mm;
	mm.start("ex5");
}

void lab4::cEx6::run()
{
	utm mm;
	mm.start("ex6");
}
