#pragma once
#include <map>
#include "exercise.h"
#include <list>

class cLab {
protected:
	std::map<int, cExercise*> exercises;
public:
	void run();
};
