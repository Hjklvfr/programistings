#include "lab.h"
#include <algorithm>
#include <iostream>
#include <map>

void cLab::run()
{
	while (true)
	{

		std::cout << "Enter number of exercise(for exit -1): ";
		int n;
		std::cin >> n;
		if (n == -1)
		{
			system("cls");
			return;
		}

		cExercise* ex = exercises[n];
		if (ex == NULL)
		{
			std::cout << "Exercise is not found" << std::endl;
			continue;
		}

		system("cls");
		ex->run();
		std::cout << std::endl;
	}
}
