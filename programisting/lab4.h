#pragma once
#include "lab.h"
//#include "CTTM.h"

//ADD_STATE(A);
//ADD_STATE(B);
//ADD_STATE(C);
//ADD_STATE(D);
//
//ADD_RULE(A, Blank, 1, Right, B);
//ADD_RULE(A, 1, 1, Left, B);
//
//ADD_RULE(B, Blank, 1, Left, A);
//ADD_RULE(B, 1, Blank, Left, C);
//
//ADD_RULE(C, Blank, 1, Right, Stop);
//ADD_RULE(C, 1, 1, Left, D);
//
//ADD_RULE(D, Blank, 1, Right, D);
//ADD_RULE(D, 1, Blank, Right, A);
//
//using tape = Tape<Blank>;
//using machine = Machine<A, 0, tape>;
//using result = Run<machine>::type;

namespace lab4
{
	class cEx1 : public cExercise {
	public:
		virtual void run() override;
	};

	class cEx2 : public cExercise {
	public:
		virtual void run() override;
	};

	class cEx3 : public cExercise {
	public:
		virtual void run() override;
	};

	class cEx4 : public cExercise {
	public:
		virtual void run() override;
	};

	class cEx5 : public cExercise {
	public:
		virtual void run() override;
	};

	class cEx6 : public cExercise {
	public:
		virtual void run() override;
	};

	class cLab4 :
		public cLab
	{
	private:
		cEx1 ex1;
		cEx2 ex2;
		cEx3 ex3;
		cEx4 ex4;
		cEx5 ex5;
		cEx6 ex6;
	public:
		cLab4();
	};
}

