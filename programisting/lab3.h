#pragma once
#include "lab.h"

namespace lab3 {
	class cEx1 : public cExercise {
	public:
		virtual void run() override;
	};

	class cLab3 :
		public cLab
	{
	private:
		cEx1 ex1;
	public:
		cLab3();
	};
}

