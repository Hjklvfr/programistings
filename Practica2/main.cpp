﻿#include <iostream>
#include <lab2.h>
#include <lab3.h>
#include <lab4.h>


int main()
{
	std::map<int, cLab*> labs;
	lab2::cLab2 lab2;
	lab3::cLab3 lab3;
	lab4::cLab4 lab4;
	labs.insert({ 2, &lab2 });
	labs.insert({ 3, &lab3 });
	labs.insert({ 4, &lab4 });
	while (true)
	{
		std::cout << "Enter number of lab(for exit -1): ";
		int n;
		std::cin >> n;
		if (n == -1)
		{
			return EXIT_SUCCESS;
		}


		auto lab = labs[n];
		if (lab == NULL)
		{
			std::cout << "Lab is not found" << std::endl;
			continue;
		}

		system("cls");
		lab->run();
	}
	return EXIT_SUCCESS;
}

